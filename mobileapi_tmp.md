﻿# Mobile API

## How to use

**NOTE:** all methods except ```/mobile/account```, ```/mobile/auth```, ```/mobile/countries```, ```/mobile/timezones``` require a token defined in the ```X-Api-Token``` HTTP header.

A new token can be created using the /mobile/auth endpoint.

A new account can be created using the /mobile/account endpoint.

New users for an existing account cannot be created through the mobile API. These must be managed through the website.

Use a job ID of 0 to log tracks without assigning to a job. Tracks created this way will appear on the track uploads page.

## Success responses

Success responses return a 200 (OK) HTTP status code. Errors typically return a 500 (Error) status code.

## Error responses

Error responses are in the following format:

```
{
    "message": "An error has occurred.",
    "exceptionMessage": "THE ACTUAL ERROR WILL GO HERE",
    "exceptionType": "THE EXCEPTION TYPE NAME CAN BE IDENTIFIED HERE"
}
```

## Endpoints

### POST /mobile/account : bool

Creates a new account (ie: sign up / register)

#### POST Data

```
{
    username: String, // The admin username for this account
    password: String, // The admin password for this account
    deviceId: String, // Device ID, MAC address, or some form of unique ID
    firstName: String, // User's first name
    lastName: String,  // User's last name
    email: String,     // User's email
    companyName: String, // Company name for the account
    countryCode: String, // Country code. See /mobile/countries
    regionId: String,    // Region code. See /mobile/countries/{code}/regions
    timezoneId: String   // Timezone ID. See /mobile/timezones
}
```

#### Returns

True on success. Error response on fail.



### POST /mobile/auth : TokenDetails

Creates an authorization token.

#### POST Data

```
{
    username: String,
    password: String,
    deviceId: String // Device ID, MAC address, or some form of unique ID
}
```

#### Returns

**TokenDetails:**
```
{
  token: String // the string token to be provided with the X-Api-Token header
}
```
Nothing on success


### GET /mobile/countries : Array of [Country](#Country)

Lists all available countries.

#### Returns

An array of [Country](#Country).

### GET /mobile/countries/{code} : [Country](#Country)

Gets the details for a single country where {code} is a 2-digit alpha code, eg US, AU, GB.

#### Returns

A [Country](#Country), or a 404 error.

### GET /mobile/countries/{code}/regions : Array of [Region](#Country)

Gets all regions for the country specified by the country matching the 2-digit alpha code {code}.

#### Returns

```
[
  {
    id: String,   // ID of the region
    name: String  // Descriptive name for the country
  }
]
```


### GET /mobile/jobs

Gets the details of all jobs.

### Returns

```
[
  {
    id: Integer,
    title: String,
    description: String,
    status: Integer,
    items: [Item],
    workers: [Worker],
    locations: [Location]
  }
]
```

Item:
```
{
    id: Integer,
    name: String
}
```

Worker:
```
{
    userId: Integer,
    jobStatus: Integer,
    firstName: String,
    lastName: String,
    lastUpdated: DateTime
}
```

Locations:
```
{
    id: Integer,
    name: String
}
```

## GET /mobile/jobs/{id}

Get the details of a job.

### Returns

```
{
    id: Integer,
    title: String,
    description: String,
    status: Integer,
    items: [Item],
    workers: [Worker],
    locations: [Location]
}
```

## GET /mobile/jobs/{id}/map

Get the job map data for a job.

### Returns

```
{
    layerId: Integer,       // nullable. the selected job map layer.
    countryCode: String,    // 2-digit ISO country code
    boundaries [Boundary]   // boundaries (areas) defined for this job 
}
```

Boundary:

```
{
    name: String,
    id: Integer,
    active: Boolean,
    polygons: [Polygon] 
}
```

Polygon:
```
{
    outerRing: Ring,
    innerRings: [Ring]
}
```

Ring:
```
{
    points: [Point]
}
```

Point:
```
{
    lat: Double,
    lng: Double
}
```



## GET /mobile/jobs/{id}/status

Get the job status of this job relative to the logged on user.

### Returns

```
Integer
```

## POST /mobile/jobs/{id}/status

Sets the job status of this job relative to the logged on user.

### POST Data

Type: application/json

```
Integer
```

## POST /mobile/jobs/{id}/deliveries

Sets the number of deliveries the user has completed for this job.

### POST Data

Type: application/json

```
Integer
```

## POST /mobile/jobs/{id}/items

Sets the items the user has on hand, or will complete for this job.

### POST Data

```
[Integer]
```
array of item IDs (see the jobs endpoint)

## POST /mobile/jobs/{id}/locations

Sets the locations the user marks completed for this job (eg: postal codes or suburbs)

```
[Integer]
```
array of item IDs (see the locations endpoint)


## POST /mobile/jobs/{id}/trackpoints

Uploads one or more points to the current track for job with ID {id}

### POST Data

```
[
  {
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    method: Integer, // if available TODO: document
    precisionType: Integer, // if available TODO: document
    precisionValue: Integer // if available TODO: document
  }
]
```

## POST /mobile/jobs/{id}/pois

Adds/attaches a POI to the current track for job with ID {id}.

*NOTE: this method only accepts a single POI, unlike "points" which accepts an array.*

### POST Data

```
{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    name: String,
    notes: String
}
```

## GET /mobile/jobs/{id}/pois

Gets the POIs attached to a job.

### Returns

```
[{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    name: String
}]
```

## POST /mobile/jobs/{id}/photos

Adds/attaches a photo to the current track for job with ID {id}.

### POST Data

Type: Multi-part form.

The post data should contain a form component (JSON metadata) and binary image component.

#### JSON (Form)

```
{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    name: String
}
```

*Note: if the image has geocode information embedded, then coordinates can be extracted from the EXIF information*

## GET /mobile/jobs/{id}/photos

Gets the photos attached to a job.

### Returns

```
[{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    name: String,
    photo: String // RELATIVE url to the photo
}]
```


## GET /mobile/timezones

Gets all timezones available.

### Returns

```
[
  {
    id: String,     // The timezone unique ID
    name: String    // Display name for the timezone
  }
]
```

## Types

### Country

```
{
    code: String,   // The ISO 2-digit alpha code for the country
    name: String    // Descriptive name for the country
}
```
