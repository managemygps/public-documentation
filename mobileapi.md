# Mobile API


## About This Documentation

This document represents request and response data as javascript objects for simplicity's sake. The API is provided using ASP.NET's Web API 2 which allows for data to be provided and requested in either JSON or XML format. 

JSON is the recommended data format to use, and the only format tested.

In addition the multipart form used for photo uploads only supports JSON for the metadata component.




## Using the API


### API Tokens

**NOTE:** all methods except ```/mobile/account```, ```/mobile/auth```, ```/mobile/countries```, ```/mobile/timezones``` require a token defined in the ```X-Api-Token``` HTTP header.

A new token can be created using the ```/mobile/auth``` endpoint.


### Request/Response Content Types

All methods can accept and return both XML and JSON content types, with the exception of the ```/mobile/jobs/{id}/photos``` endpoint which accepts a [multi-part form](https://www.ietf.org/rfc/rfc2388.txt) containing JSON and binary data.

To send JSON set the *Content-Type* HTTP header to *application/json*: ```Content-Type: application/json```

To request JSON responses, set the *Accept* HTTP header to *application/json*: ```Accept: application/json```


### Account & User Creation

A new account can be created using the ```/mobile/account``` endpoint.

New users for an existing account cannot be created through the mobile API. These must be managed through the website.


### Job IDs

Use a **job ID of 0** to create tracks without assigning to a job. Tracks created this way will appear on the track uploads page.




## Success responses

Success responses return a 200 (OK) or other 2xx HTTP status code.




## Error responses

Application errors typically return a 4xx status code. These include things such as validation errors, invalid state exceptions, etc. The most common error is a 400 (Bad Request). Other common error codes are 404 (Not Found), 403 (Not Authorized).

Unexpected errors typically return a 5xx HTTP status code. The most common being a 500 (Internal Server Error).

In addition to the status code, the contents of a response contain details of the exception including the message and type.

Error responses are in the following format:

```javascript
{
    code: Integer, // error code
    message: String, // high level error message
    exceptionMessage: String, // the actual exception message
    exceptionType: String // the exception type represented as a string
}
```




## Endpoints




### POST /mobile/account : [StatusResponse](#markdown-header-statusresponse)

Creates a new account (ie: sign up / register)

*Send* a [Registration](#markdown-header-registration)

*Returns* a [StatusResponse](#markdown-header-statusresponse)




### POST /mobile/auth : [TokenDetails](#markdown-header-tokendetails)

Creates an authorization token.

*Send* a [Credentials](#markdown-header-credentials)

*Returns* a [TokenDetails](#markdown-header-tokendetails)




### GET /mobile/countries : array of [Country](#markdown-header-country)

Lists all available countries.

*Returns* an array of [Country](#markdown-header-country).




### GET /mobile/countries/{code} : [Country](#markdown-header-country)

Gets the details for a single country where {code} is a 2-digit alpha code, eg US, AU, GB.

*Returns* a [Country](#markdown-header-country).




### GET /mobile/countries/{code}/regions : array of [Region](#markdown-header-region)

Gets all regions for the country specified by the country matching the 2-digit alpha code {code}.

*Returns* an array of [Region](#markdown-header-region)




### GET /mobile/jobs : array of [Job](#markdown-header-job)

Gets the details of all jobs.

*Returns* an array of [Job](#markdown-header-job)




### GET /mobile/jobs/{id} : [Job](#markdown-header-job)

Get the details of a job.

*Returns* a [Job](#markdown-header-job)




### GET /mobile/jobs/{id}/status : [JobStatus](#markdown-header-jobstatus)

Get the job status of this job relative to the authenticated user.

*Returns* a [JobStatus](#markdown-header-jobstatus)




### POST /mobile/jobs/{id}/status : [JobStatus](#markdown-header-jobstatus)

Sets the job status of this job relative to the authenticated user.

*Send a [JobStatus](#markdown-header-jobstatus)*

*Returns* a [StatusResponse](#markdown-header-statusresponse)



### POST /mobile/jobs/{id}/deliveries : [JobStatus](#markdown-header-jobstatus)

Sets the number of deliveries the user has completed for this job.

*Send* a positive ```Integer``` to indicate the number of deliveries handled in this job

*Returns* a [StatusResponse](#markdown-header-statusresponse)




### POST /mobile/jobs/{id}/items : [StatusResponse](#markdown-header-statusresponse)

Sets the items the user has on hand, or will complete for this job.

*Send* an array of ```Integer``` item IDs.

Item IDs available for a job are available with the job details (see [jobs](#markdown-header-get-mobilejobs-array-of-job) and [job](#markdown-header-get-mobilejobsid-job))

*Returns* a [StatusResponse](#markdown-header-statusresponse)




### POST /mobile/jobs/{id}/locations : [StatusResponse](#markdown-header-statusresponse)

Sets the locations the user marks completed for this job (eg: postal codes or suburbs)

*Send* an array of ```Integer``` location IDs.

*Returns* a [StatusResponse](#markdown-header-statusresponse)




### POST /mobile/jobs/{id}/trackpoints : [StatusResponse](#markdown-header-statusresponse)

Uploads one or more points to the current track for job with ID {id}

*Send* an array of [TrackPoint](#markdown-header-trackpoint)

*Returns* a [StatusResponse](#markdown-header-statusresponse)




### POST /mobile/jobs/{id}/pois : [StatusResponse](#markdown-header-statusresponse)

**TODO:Not updating counter**

Adds/attaches a POI to the current track for job with ID {id}.

*NOTE: this method only accepts a single POI, unlike "points" which accepts an array.*

*Send* a [TrackPoi](#markdown-header-trackpoi)

*Returns* a [StatusResponse](#markdown-header-statusresponse)




### POST /mobile/jobs/{id}/photos : [StatusResponse](#markdown-header-statusresponse)

Adds/attaches a photo to the current track for job with ID {id}.

*Note: this method uses a multi-part form to submit the binary photo data + text metadata in a single HTTP request.*
For more info on submitting a photo using this method see [this section](#markdown-header-multipart-form-photo-uploads).

*Send* a multi-part form containing a [TrackPhoto](#markdown-header-trackphoto) (encoded as json in the form part) and single file component containing the binary image.

*Note: if the image has geocode information embedded, then coordinates **can** be extracted from the EXIF information*

*Returns* a [StatusResponse](#markdown-header-statusresponse)




### GET /mobile/timezones : array of [Timezone](#markdown-header-timezone)

Gets all timezones available.

*Returns* an array of [Timezone](#markdown-header-timezone)



















## Types



### StatusResponse

```javascript
{
    success: Boolean  // true indicates a success response
}
```


### Country

```javascript
{
    code: String,   // The ISO 2-digit alpha code for the country
    name: String    // Descriptive name for the country
}
```


### Credentials

```javascript
{
    username: String,
    password: String,
    deviceId: String // Device ID, MAC address, or some form of unique ID
}
```


### Job

```javascript
{
    id: Integer,
    title: String,
    description: String,
    status: Integer,
    items: [JobItem],
    workers: [JobWorker],
    locations: [JobLocation]
}
```
See also: [JobItem](#markdown-header-jobitem), [JobWorker](#markdown-header-jobworker), [JobLocation](#markdown-header-joblocation).


### JobItem

```javascript
{
    id: Integer,
    description: String,
    quantity: Integer,
    client: String
}
```


### JobWorker

```javascript
{
    userId: Integer,
    jobStatus: Integer,
    firstName: String,
    lastName: String
}
```


### JobLocation

```javascript
{
    id: Integer,
    name: String
}
```


### JobStatus

JobStatus is an enum containing the following values

```javascript
Unknown = -1,
NotStarted = 0,
InProgress = 1,
Paused = 2,
Finished = 3
```


### Region

```javascript
{
    id: String,   // ID of the region
    name: String  // Descriptive name for the country
}
```


### Registration

```javascript
{
    username: String, // The admin username for this account
    password: String, // The admin password for this account
    deviceId: String, // Device ID, MAC address, or some form of unique ID
    firstName: String, // User's first name
    lastName: String,  // User's last name
    email: String,     // User's email
    companyName: String, // Company name for the account
    countryCode: String, // Country code. See /mobile/countries
    regionId: String,    // Region code. See /mobile/countries/{code}/regions
    timezoneId: String   // Timezone ID. See /mobile/timezones
}
```


### Timezone

```javascript
{
    id: String,     // The timezone unique ID
    name: String    // Display name for the timezone
}
```



### TokenDetails

```javascript
{
    token: String // the string token to be provided with the X-Api-Token header
}
```


### TrackPhoto
```javascript
{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    name: String
}
```


### TrackPhotoPoi

```javascript
{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    name: String,
    photo: String // RELATIVE url to the photo
}
```



### TrackPoi

```javascript
{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    name: String
}
```



### TrackPoi
```javascript
{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    name: String,
    notes: String
}
```


### TrackPoint

```javascript
{
    time: Date,
    lat: Double,
    lng: Double,
    alt: Double,
    method: Integer, // if available TODO: document
    precisionType: Integer, // if available TODO: document
    precisionValue: Integer // if available TODO: document
}
```


## Other Notes

### Multipart Form Photo Uploads

Photos submitted as multipart forms should include the photo's metadata sent as a JSON component. The metadata can either be sent as:

1. A JSON string supplied as the first form content component of the POST.
2. A file attachment component with content type of ```application/json```.

**Example POST 1. including HTTP headers:**
```
POST http://apiserver/mobile/jobs/0/photos HTTP/1.1
Host: apiserver
Connection: keep-alive
Content-Length: 16314
Accept: application/json
Cache-Control: no-cache
Origin: chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm
User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryVJtXpzr9IcbrOQNA
Accept-Encoding: gzip, deflate
Accept-Language: en-GB,en-US;q=0.8,en;q=0.6

------WebKitFormBoundaryVJtXpzr9IcbrOQNA
Content-Disposition: form-data; name="metadata"

{"lat":33.333, "lng":144.123, "alt":10.0, "name":"Test Photo", "time":"2015-02-03T07:48:52.540Z"}
------WebKitFormBoundaryVJtXpzr9IcbrOQNA
Content-Disposition: form-data; name="image"; filename="image.jpg"
Content-Type: image/jpg

...///encoded image///

------WebKitFormBoundaryVJtXpzr9IcbrOQNA--
```

**Example POST 2. including HTTP headers:**
```
POST http://apiserver/mobile/jobs/0/photos HTTP/1.1
Host: apiserver
Connection: keep-alive
Content-Length: 16314
Accept: application/json
Cache-Control: no-cache
Origin: chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm
User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryVJtXpzr9IcbrOQNA
Accept-Encoding: gzip, deflate
Accept-Language: en-GB,en-US;q=0.8,en;q=0.6

------WebKitFormBoundaryVJtXpzr9IcbrOQNA
Content-Disposition: form-data; name="metadata"; filename="metadata.json"
Content-Type: application/json

{"lat":33.333, "lng":144.123, "alt":10.0, "name":"Test Photo", "time":"2015-02-03T07:48:52.540Z"}
------WebKitFormBoundaryVJtXpzr9IcbrOQNA
Content-Disposition: form-data; name="image"; filename="image.jpg"
Content-Type: image/jpg

...{encoded image}

------WebKitFormBoundaryVJtXpzr9IcbrOQNA--
```
